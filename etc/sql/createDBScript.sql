-- 
-- script executes by postgress ROOT user, logged in any DB.
-- 
DROP SCHEMA IF EXISTS vfl_database;
DROP DATABASE  IF EXISTS vfl_database;
DROP USER IF EXISTS vfl_user, vfl_admin;


CREATE DATABASE vfl_database;


CREATE USER vfl_user  WITH PASSWORD 'p_vfl_user';
CREATE USER vfl_admin WITH PASSWORD 'p_vfl_admin';

REVOKE CONNECT ON DATABASE vfl_database FROM PUBLIC;
GRANT CONNECT ON DATABASE vfl_database  TO vfl_user;
GRANT CONNECT ON DATABASE vfl_database  TO vfl_admin;

GRANT ALL PRIVILEGES ON DATABASE vfl_database to vfl_admin;

commit;